aiofiles
aiohttp<3.8
aiohttp-middlewares
alembic<1.7 # type-check fails with 1.7.1
boto3
boto3-stubs<=1.17.90 # server/actioncache/caches/s3_cache.py: "error: TypedDict "ClientErrorResponseTypeDef" has no key 'Error'"
botocore
Click
coverage
cryptography>=38.0.0 # For compatibility with pyopenssl>=22.0.0 https://github.com/pyca/pyopenssl/issues/1143
cryptography # Required by pyjwt for RSA
# Dependency management
dnspython
ecdsa<0.15    # python-jose 3.1.0 is not compatible with 0.15
fakeredis<=1.7.4
flask
flask-cors
grpcio>=1.33.2
grpcio-reflection
grpc-stubs<1.24.7 # 1.24.7 depends on mypy>=0.902 (conflicting with above mypy<=0.812 constraint)
hiredis # Faster parsing of redis response.
janus>=0.6.2
jinja2==3.0.3
jsonschema>=3.0.0
lark-parser
moto>=4.0.2 # https://github.com/spulec/moto/pull/5417 and https://github.com/spulec/moto/pull/5438
mypy<=0.982
pika
pip-tools
protobuf<3.20.0  # https://github.com/protocolbuffers/protobuf/issues/9730
psutil
psycopg2-binary
py
pycodestyle
pycurl
PyJWT<2.0.0 # Version 2.0.0 includes breaking changes https://github.com/jpadilla/pyjwt/blob/master/CHANGELOG.md#v200
pylint<2.9.0
pyopenssl>=22.0.0 # For compatibility with cryptography>=38.0.0 https://github.com/pyca/pyopenssl/issues/1143
pytest
pytest-aiohttp
pytest-asyncio
pytest-cov
pytest-forked
pytest-pycodestyle
pytest-pylint<0.15.0 #https://github.com/carsongee/pytest-pylint/issues/137
pytest-xdist
PyYAML
redis<=4.1.4 # Later versions cause aync-timeout version conflicts with aiohttp and aiohttp-middlewares
requests<=2.24.0
Sphinx<4
sphinx-click
sphinxcontrib-apidoc
sphinxcontrib-napoleon
sphinx-rtd-theme<=0.4.3 # For HTML search fix (upstream #672)
sqlalchemy2-stubs<=0.0.2a22 # For _ConnectionFairy.dbapi_connection issue caused by 0.0.2.a23
SQLAlchemy[mypy]>=1.4.24 # For _ConnectionFairy.dbapi_connection
# Style
# Testing
testing.postgresql
testing.rabbitmq
# Type checking
werkzeug<2.2.0
