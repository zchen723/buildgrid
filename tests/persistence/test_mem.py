# Copyright (C) 2019 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# pylint: disable=redefined-outer-name


from datetime import datetime
import os
import tempfile
from typing import Dict, List
from buildgrid.utils import hash_from_dict

import pytest

from buildgrid._enums import LeaseState, MetricCategories, OperationStage
from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Action, Digest
from buildgrid._protos.google.devtools.remoteworkers.v1test2 import bots_pb2
from buildgrid._protos.google.longrunning import operations_pb2
from buildgrid.server.cas.storage import lru_memory_cache
from buildgrid.server.job import Job
from buildgrid.server.persistence.mem.impl import MemoryDataStore


@pytest.fixture()
def datastore():
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)
    data_store = MemoryDataStore(storage)
    data_store.activate_monitoring()
    data_store.set_instance_name('test_instance')
    data_store.set_action_browser_url('https://localhost')
    yield data_store
    data_store.watcher_keep_running = False


def populate_datastore(datastore):
    datastore.create_job(Job(
        do_not_cache=True,
        action=Action(do_not_cache=True),
        action_digest=Digest(hash="test-action", size_bytes=100),
        name="test-job",
        priority=1,
        stage=OperationStage.CACHE_CHECK,
        operation_names=[],
        platform_requirements={"OSFamily": set(["solaris"])}
    ))
    datastore.create_operation("test-operation", "test-job")

    datastore.queue_job("test-job")

    datastore.create_job(Job(
        do_not_cache=True,
        action=Action(do_not_cache=True),
        action_digest=Digest(hash="other-action", size_bytes=10),
        name="other-job",
        priority=5,
        stage=OperationStage.CACHE_CHECK,
        operation_names=[],
        platform_requirements={"OSFamily": set(["linux"])}
    ))
    datastore.create_operation("other-operation", "other-job")
    datastore.queue_job("other-job")

    datastore.create_job(Job(
        do_not_cache=True,
        action=Action(do_not_cache=True),
        action_digest=Digest(hash="extra-action", size_bytes=50),
        name="extra-job",
        priority=10,
        stage=OperationStage.CACHE_CHECK,
        operation_names=[],
        platform_requirements={"OSFamily": set(["linux"])}
    ))
    datastore.create_operation("extra-operation", "extra-job")
    datastore.queue_job("extra-job")

    datastore.create_job(Job(
        do_not_cache=True,
        action=Action(do_not_cache=True),
        action_digest=Digest(hash="chroot-action", size_bytes=50),
        name="chroot-job",
        priority=10,
        stage=OperationStage.CACHE_CHECK,
        operation_names=[],
        platform_requirements={
            "OSFamily": set(["aix"]),
            "ChrootDigest": set(["space", "dead-beef"]),
            "ISA": set(["x64", "x32"]),
        }
    ))
    datastore.create_operation("chroot-operation", "chroot-job")
    datastore.queue_job("chroot-job")


def test_action_browser_url_set(datastore):
    job = Job(
        do_not_cache=True,
        action=Action(do_not_cache=True),
        action_digest=Digest(hash="action_test", size_bytes=50),
        name="chroot-job",
        priority=10,
        stage=OperationStage.CACHE_CHECK,
        operation_names=[],
        platform_requirements={
            "OSFamily": set(["aix"]),
            "ChrootDigest": set(["space", "dead-beef"]),
            "ISA": set(["x64", "x32"]),
        }
    )
    datastore.create_job(job)
    digest = job.action_digest.hash
    size = job.action_digest.size_bytes
    assert job.execute_response.message == f'https://localhost/action/test_instance/{digest}/{size}/'


@pytest.mark.parametrize("do_not_cache", [False, True])
def test_job_do_not_cache_attribute_from_action(datastore, do_not_cache):
    job = Job(
        do_not_cache=do_not_cache,
        action=Action(do_not_cache=do_not_cache),
        action_digest=Digest(hash="action_test", size_bytes=50),
        name="test-job-0"
    )
    datastore.create_job(job)
    assert job.do_not_cache == do_not_cache

def test_deactivate_monitoring(datastore):
    populate_datastore(datastore)
    assert datastore.is_instrumented

    operation = operations_pb2.Operation(
        name="test-operation-2",
        done=False
    )

    datastore.create_operation(operation.name, "test-job")
    assert "test-operation-2" in datastore.jobs_by_operation
    assert "test-job" in datastore.operations_by_stage[OperationStage(
        1)]

    datastore.deactivate_monitoring()
    assert datastore.operations_by_stage == {}


def test_delete_job(datastore):
    populate_datastore(datastore)
    datastore.delete_job("test-job")
    assert "test-job" not in datastore.jobs_by_name


def test_assign_n_leases(datastore):
    populate_datastore(datastore)

    assigned_jobs = {}
    def assignment_callback(jobs: List[Job]) -> Dict[str, Job]:
        job_map = {}
        for job in jobs:
            lease = job.lease
            if not lease:
                lease = job.create_lease("test-suite", data_store=datastore)
            else:
                job.worker_name = "test-suite"
            if lease:
                job.mark_worker_started()
                job_map[job.name] = job
        assigned_jobs.update(job_map)
        return job_map

    # Request 2 Leases from the data store. The example data contains
    # 2 queued jobs which have `OSFamily: linux`, so we should get two
    # jobs passed to the assignment callback as we requested.
    capability = hash_from_dict({"OSFamily": ["linux"]})
    datastore.assign_n_leases(
        lease_count=2,
        capability_hash=capability,
        assignment_callback=assignment_callback
    )
    assert len(assigned_jobs) == 2

    # Clear the assigned jobs dict so we can test a bit more
    assigned_jobs.clear()
    assert len(assigned_jobs) == 0

    # Check that those jobs actually got assigned in the data store too.
    # There should be no matching jobs left when we ask for more leases.
    datastore.assign_n_leases(
        lease_count=10,
        capability_hash=capability,
        assignment_callback=assignment_callback
    )
    assert len(assigned_jobs) == 0


def test_assign_n_leases_limits(datastore):
    populate_datastore(datastore)

    assigned_jobs = {}
    def assignment_callback(jobs: List[Job]) -> Dict[str, Job]:
        job_map = {}
        for job in jobs:
            lease = job.lease
            if not lease:
                lease = job.create_lease("test-suite", data_store=datastore)
            else:
                job.worker_name = "test-suite"
            if lease:
                job.mark_worker_started()
                job_map[job.name] = job
        assigned_jobs.update(job_map)
        return job_map

    # Request only 1 lease from the data store. The example data contains
    # 2 queued jobs matching `OSFamily: linux`, so we should get the job
    # with the highest priority (`other-job`).
    capability = hash_from_dict({"OSFamily": ["linux"]})
    datastore.assign_n_leases(
        lease_count=1,
        capability_hash=capability,
        assignment_callback=assignment_callback
    )
    assert len(assigned_jobs) == 1
    assert "other-job" in assigned_jobs

    # Clear the assigned jobs dict so we can test further easily
    assigned_jobs.clear()
    assert len(assigned_jobs) == 0

    # Request many leases from the data store. There's only one job left
    # in the queue, so we should get just that job (`extra-job`).
    datastore.assign_n_leases(
        lease_count=10,
        capability_hash=capability,
        assignment_callback=assignment_callback
    )
    assert len(assigned_jobs) == 1
    assert "extra-job" in assigned_jobs


# Check that the metrics returned match what is
# populated in the test datastore
def test_get_metrics(datastore):
    populate_datastore(datastore)

    # Setup expected counts for each category
    expected_metrics = {}
    expected_metrics[MetricCategories.LEASES.value] = {
        LeaseState.UNSPECIFIED.value: 0,
        LeaseState.PENDING.value: 0,
        LeaseState.COMPLETED.value: 0,
        LeaseState.CANCELLED.value: 0,
        LeaseState.ACTIVE.value: 0
    }
    expected_metrics[MetricCategories.JOBS.value] = {
        OperationStage.UNKNOWN.value: 0,
        OperationStage.CACHE_CHECK.value: 4,
        OperationStage.QUEUED.value: 0,
        OperationStage.EXECUTING.value: 0,
        OperationStage.COMPLETED.value: 0
    }

    returned_metrics = datastore.get_metrics()
    assert returned_metrics != {}
    for category in MetricCategories:
        assert category.value in returned_metrics
        assert expected_metrics[category.value] == returned_metrics[category.value]

def test_get_request_metadata(datastore):
    assert datastore.get_operation_request_metadata_by_name('foo-op') is None
