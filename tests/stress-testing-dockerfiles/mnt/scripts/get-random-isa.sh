#!/bin/bash
# This script returns a random value to use as the ISA

isa[0]="FakeISA_0"
isa[1]="FakeISA_1"

len=${#isa[@]}
rand=$(($RANDOM % $len))
echo ${isa[$rand]}

