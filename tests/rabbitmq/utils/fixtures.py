# Copyright (C) 2022 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from time import sleep

import pytest

try:
    import pika
    import testing.rabbitmq
except ImportError:
    print("Failed to import RabbitMQ test dependencies")


@pytest.fixture
def rabbitmq_server():
    with testing.rabbitmq.RabbitMQServer(boot_timeout=60.0) as server:
        yield server

    # Give the process time to finish before relaunching it for the
    # next test.
    sleep(1)


@pytest.fixture
def connection_params(rabbitmq_server):
    connection_parameters = pika.ConnectionParameters(**rabbitmq_server.dsn())
    yield connection_parameters
