# Copyright (C) 2022 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import pytest

rabbitmq_exception = None

try:
    import pika
    import testing.rabbitmq
    with testing.rabbitmq.RabbitMQServer(boot_timeout=60.0) as server:
        PIKA_REQUIREMENTS_INSTALLED = True
except Exception as e:
    print(f"Required dependency not found. Skipping pika unit tests: {e}")
    rabbitmq_exception = e
    PIKA_REQUIREMENTS_INSTALLED = False

check_rabbitmq_server_is_installed = pytest.mark.skipif(not PIKA_REQUIREMENTS_INSTALLED,
                                                        reason=f"RabbitMQ requirements not found: {rabbitmq_exception}.")
