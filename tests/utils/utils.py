# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import asyncio
import multiprocessing
import psutil
import sys
import traceback
from typing import Callable, Optional

import grpc


def kill_process_tree(pid):
    proc = psutil.Process(pid)
    children = proc.children(recursive=True)

    def kill_proc(p):
        try:
            p.kill()
        except psutil.AccessDenied:
            # Ignore this error, it can happen with
            # some setuid bwrap processes.
            pass

    # Bloody Murder
    for child in children:
        kill_proc(child)
    kill_proc(proc)


def read_file(file_path, text_mode=False):
    with open(file_path, 'r' if text_mode else 'rb') as in_file:
        return in_file.read()


def run_in_subprocess(function, *arguments):
    def subprocess_wrapper(queue, *args):
        try:
            result = function(*args)
        except:
            try:
                exception = sys.exc_info()[1]
                tb = traceback.format_exc()
                if isinstance(exception, grpc.RpcError):
                    # RpcError may contain state that is not picklable
                    exception = grpc.RpcError(str(exception))
                queue.put((None, exception, tb))
            except Exception as e:
                queue.put((None, e, 'wat'))
        else:
            queue.put((result, None, None))

    result_queue = multiprocessing.Queue()
    # Use subprocess to avoid creation of gRPC threads in main process
    # See https://github.com/grpc/grpc/blob/master/doc/fork_support.md
    process = multiprocessing.Process(target=subprocess_wrapper,
                                      args=(result_queue, *arguments))

    try:
        process.start()
        result, exception, tb = result_queue.get()
        process.join()

    except KeyboardInterrupt:
        kill_process_tree(process.pid)
        raise

    if exception is not None:
        sys.stderr.write(tb)
        raise exception

    return result


def run_coroutine_in_subprocess(coro: Callable) -> Optional[bool]:
    """Run a coroutine in an event loop in a multiprocessing.Process.

    This utility method orchestrates the execution of an asyncio coroutine
    in a subprocess, using multiprocessing.Process to fork. This allows
    tests to run asyncio-based gRPC code without causing gRPC threads to be
    created in the main process.

    This is important since it preserves the ability of other tests run by
    the same executor to fork for e.g. running a BuildGrid server.

    Args:
        coro (Callable): The coroutine function to execute and schedule
            in a subprocess.

    """
    def _subprocess():
        try:
            loop = asyncio.get_event_loop()
            loop.run_until_complete(coro())
            return True
        except Exception:
            traceback.print_exc()
        return False
    return run_in_subprocess(_subprocess)
