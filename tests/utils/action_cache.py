# Copyright (C) 2019 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from concurrent import futures
from contextlib import contextmanager
import multiprocessing
import os
import signal
import traceback

import grpc
import pytest_cov

from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid.server.actioncache.service import ActionCacheService
from buildgrid.server.actioncache.instance import ActionCache
from buildgrid.server.actioncache.caches.lru_cache import LruActionCache
from buildgrid.server.capabilities.service import CapabilitiesService
from buildgrid.server.capabilities.instance import CapabilitiesInstance
from buildgrid.server.cas.storage.remote import RemoteStorage

from .cas import serve_cas


@contextmanager
def serve_cache(instances, num_failures=0, allow_updates=True):
    exc = None
    try:
        server = TestServer(instances, num_failures, allow_updates)
        yield server
    except Exception as e:
        exc = e
    finally:
        try:
            server.quit()
        except UnboundLocalError:
            pass
        if exc is not None:
            raise exc


def should_fail(method_name, failure_dict, num_failures):
    if method_name not in failure_dict:
        failure_dict[method_name] = 0

    if failure_dict[method_name] >= num_failures:
        failure_dict[method_name] = 0
        return False
    else:
        failure_dict[method_name] += 1
        return True


class FailableActionCacheService(ActionCacheService):
    def __init__(self, server, num_failures):
        super().__init__(server)

        self.__failure_dict = {}
        # The number of times to fail each call before succeeding
        self.num_failures = num_failures

    def GetActionResult(self, request, context):
        method_name = "GetActionResult"
        if should_fail(method_name, self.__failure_dict, self.num_failures):
            context.set_code(grpc.StatusCode.UNAVAILABLE)
            return remote_execution_pb2.ActionResult()
        else:
            return super().GetActionResult(request, context)

    def UpdateActionResult(self, request, context):
        method_name = "UpdateActionResult"
        if should_fail(method_name, self.__failure_dict, self.num_failures):
            context.set_code(grpc.StatusCode.UNAVAILABLE)
            return remote_execution_pb2.ActionResult()
        else:
            return super().UpdateActionResult(request, context)


class TestServer:

    def __init__(self, instances, num_failures, allow_updates):
        self.instances = instances

        self.__queue = multiprocessing.Queue()
        self.__process = multiprocessing.Process(
            target=TestServer.serve,
            args=(self.__queue, self.instances, num_failures, allow_updates))
        self.__process.start()

        self.port = self.__queue.get()
        if self.port == "ERROR":
            raise Exception("Error raised during server startup, check stderr")
        self.remote = f'http://localhost:{self.port}'

    @classmethod
    def serve(cls, queue, instances, num_failures, allow_updates):
        pytest_cov.embed.cleanup_on_sigterm()

        try:
            # `serve_cas()` forks, must be called before gRPC is initialized
            with serve_cas(['testing']) as cas:
                # Use max_workers default from Python 3.4+
                max_workers = (os.cpu_count() or 1) * 5
                server = grpc.server(futures.ThreadPoolExecutor(max_workers))
                port = server.add_insecure_port('localhost:0')

                storage = RemoteStorage(cas.remote, 'testing')
                storage.setup_grpc()

                ac_service = FailableActionCacheService(server,
                                                        num_failures=num_failures)
                capabilities_service = CapabilitiesService(server)
                for name in instances:
                    cache = LruActionCache(storage, 256, allow_updates=allow_updates)
                    ac_instance = ActionCache(cache)
                    capabilities_instance = CapabilitiesInstance(action_cache_instance=ac_instance)
                    capabilities_service.add_instance(name, capabilities_instance)
                    ac_service.add_instance(name, ac_instance)

                server.start()
                queue.put(port)

                signal.pause()
        except:
            traceback.print_exc()
            queue.put("ERROR")

    def quit(self):
        if self.__process:
            self.__process.terminate()
            self.__process.join()
