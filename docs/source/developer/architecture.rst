.. _architecture:

Architecture
============

This section provides details of the overall BuildGrid architecture.

.. toctree::
   :maxdepth: 3

   architecture_overview.rst
   data_model.rst

The architecture is planned to change in the coming months, to improve
performance by removing the dependency on a central state store.

.. toctree::
   :maxdepth: 3

   rabbitmq_architecture_overview.rst
